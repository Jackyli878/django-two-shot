from django.urls import path
from receipts.views import (
    receipts_list,
    create_receipt,
    category_list,
    account_list,
    create_category,
    create_account,
)
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path("", login_required(receipts_list), name="home"),
    path(
        "create/",
        login_required(create_receipt),
        name="create_receipt",
    ),
    path("categories/", login_required(category_list), name="categories"),
    path("accounts/", login_required(account_list), name="accounts"),
    path(
        "categories/create/",
        login_required(create_category),
        name="create_category",
    ),
    path(
        "accounts/create/",
        login_required(create_account),
        name="create_account",
    ),
]
